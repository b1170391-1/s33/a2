
const Course = require ('./../models/Course')

module.exports.createCourse = (reqBody) => {

	let newCourse = new Course ({

		courseName: reqBody.courseName,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then( (result, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}


module.exports.getCourse = (name) => {
	return Course.findOne({name: name}). then (
		result => result)
}

module.exports.activeCourse = () => {
	return Course.find({isActive: true}).then((result, err) => {
			if(err) {
				return false
			} else {
				return result
			}
		})
}

//get a specific course
module.exports.specCourse = (courseName) => {
	return Course.findOne({courseName: courseName}). then (
		result => result)
} 

//get specific course by ID
module.exports.getById = (params) => {

	//look for matching document

	//return matching document

	//return error
	return Course.findById (params).then (result => result)
}

module.exports.updateStatus = (params) => {
	return Course.findByIdAndUpdate (params, {isActive: false}, {new:true}).then (result => result)
}

module.exports.unarchive = (params) => {
	return Course.findByIdAndUpdate (params, {isActive: true}, {new:true}).then (result => result)
}

module.exports.deleteOneCourse = (courseName) => {
	return Course.findOneAndDelete({courseName: courseName}). then (
		result => result)
}

module.exports.deleteById = (params, courseName) => {
	return Course.findByIdAndDelete (params, {courseName: courseName}). then (result => result)
}

