
const User = require ('./../models/User.js');

const bcrypt = require ("bcrypt");

const auth = require ("./../auth");

module.exports.checkEmail = (email) => {

	return User.findOne({email:email}).then (result => result)
}

module.exports.register = (reqBody) => {

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo

	})
	return newUser.save().then ( (result,error) => {
		if (result) {
			return true
		} else {
			return error
		}
	})
}

module.exports.getAllUsers = () => {

	return User.find().then ( (result, error) => {
		if (result) {
			return result
		}else {
			return error
		}
	})
}

module.exports.login = (reqBody) => {
	const {email, password} = reqBody
	return User.findOne({email:email}).then ( (result, error) => {
		if (result == null){
			return false
		} else {
			// what if we found the email existing but the pw is incorrect

			let isPasswordCorrect = bcrypt.compareSync(password, result.password)

			if (isPasswordCorrect == true){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

module.exports.getProfile = (data) => {

	const {id} = data 
	return User.findById(id).then ((result, err) => {
		console.log (result)

		if (result != null) {
			result.password = "";
			return result
		} else {
			return false
		}
	})
}