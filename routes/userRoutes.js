
const express = require ("express");
const router = express.Router();

const userController = require('./../controllers/userControllers.js');

const auth = require("./../auth")

//check if email exist

router.get("/email-exist", (req,res) => {
	userController.checkEmail(req.body).then ( result => res.send (result))
})

//register user
//http://localhost:4000/api/users
router.post("/register", (req,res) => {

	userController.register(req.body).then ( result => res.send (result))

})

router.get ("/", (req,res) => {
	userController.getAllUsers().then ( result => res.send (result))
})


router.post("/login", (req,res) => {

	userController.login(req.body).then (result => res.send (result))

})

//retrieve user information
router.get ("/details", auth.verify, (req,res) => {
	//id:
	//email
	//isAdmin
	let userData = auth.decode (req.headers.authorization);

	console.log (userData)

	userController.getProfile(userData, req.body).then(result => res.send(result))
})




module.exports = router;