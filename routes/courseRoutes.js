
const express = require ("express");
const router = express.Router();

const courseController = require('./../controllers/courseControllers.js');

const auth = require ("./../auth")

//create a course
router.post ("/create-course", auth.verify, (req,res) => {
	courseController.createCourse(req.body). then (result => res.send(result))
})

router.get ("/getall-course", (req,res) => {
	courseController.getCourse (req.body).then (result => res.send (result))
})

module.exports = router;


router.get ("/active-courses", auth.verify, (req, res) => {
	courseController.activeCourse (req.body).then (result => res.send (result))
})


//get specific course using findOne()

router.get ("/specific-course", auth.verify, (req,res) => {

	courseController.specCourse (req.body.courseName).then (result => res.send (result))

})

//get specific course using findbyId()

router.get ("/:id", (req,res) => {
	console.log (req.params);

	courseController.getById(req.params.id).then (result => res.send(result))
})

//update isActive status of the course using findByIDAndUpdate()

router.put ("/:id/update-status", auth.verify, (req,res) => {
	courseController.updateStatus(req.params.id).then (result => res.send(result))
})

router.put ("/:id/unarchive", (req,res) => {
	courseController.unarchive(req.params.id).then (result => res.send(result))
})

//delete course using findOneAndDelete()

router.delete ("/delete-onecourse", auth.verify, (req,res) => {
	courseController.deleteOneCourse (req.body.courseName).then (result => res.send(result))
})

//update isActive status of the course using findByIdAndDelete()

router.delete ("/:id/delete-idcourse", auth.verify, (req,res) => {
	courseController.deleteById (req.params.id).then (result => res.send(result))
})



module.exports = router;